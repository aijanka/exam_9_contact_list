import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {applyMiddleware, createStore} from "redux";
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import thunk from 'redux-thunk';
import reducer from "./store/reducer";
import axios from 'axios';

axios.defaults.baseURL = 'https://getpizzaapp.firebaseio.com/';

const store = createStore(reducer, applyMiddleware(thunk));

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
