import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import Navigation from "./components/Navigation/Navigation";
import Contacts from "./containers/Contacts/Contacts";
import AddContact from "./containers/AddContact/AddContact";
import EditContact from "./containers/EditContact/EditContact";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Navigation/>
                <Switch>
                    <Route path='/contacts' exact component={Contacts}/>
                    <Route path='/addContact' exact component={AddContact}/>
                    <Route path='/contacts/:id/editContact'  exact render={(props => (<EditContact {...props}/>))}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
