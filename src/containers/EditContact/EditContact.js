import React, {Component} from 'react';
import axios from 'axios';
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {editContact} from "../../store/actions";

import './EditContact.css';

class EditContact extends Component {
    key = this.props.match.params.id;

    state = {
        name: this.props.contacts[this.key].name,
        number: this.props.contacts[this.key].number,
        email: this.props.contacts[this.key].email,
        img: this.props.contacts[this.key].img
    };

    saveCurrentValue = event => {
        event.preventDefault();
        this.setState({[event.target.name]: event.target.value});
        console.log(event.target.value, 'value changed');
    };

    render () {
        return (
            <form className="EditContact">
                <div className="InputItem">
                    <label>Name</label>
                    <input
                        type="text"
                        name='name'
                        placeholder="Enter contact's name"
                        value={this.state.name}
                        onChange={this.saveCurrentValue}
                    />
                </div>



                <div className="InputItem">
                    <label>Phone number</label>
                    <input
                        type="number"
                        name='number'
                        placeholder="Enter contact's phone number"
                        value={this.state.number}
                        onChange={this.saveCurrentValue}
                    />
                </div>



                <div className="InputItem">
                    <label>Email</label>
                    <input
                        type="email"
                        name='email'
                        placeholder="Enter contact's email"
                        value={this.state.email}
                        onChange={this.saveCurrentValue}
                    />
                </div>



                <div className="InputItem">
                    <label>Image</label>
                    <input
                        type="text"
                        name='img'
                        placeholder="Enter contact's img"
                        value={this.state.img}
                        onChange={this.saveCurrentValue}
                    />
                </div>


                {this.state.img ? (<img className='ContactImg' src={this.state.img}/>) : <div className="emptyImg"/>}

                <div className="btns">
                    <button onClick={(event) => {
                        event.preventDefault();
                        this.props.editContact(this.key, this.state);
                        this.props.history.push('/contacts');
                    }}>
                        Edit contact
                    </button>
                    <NavLink to='/contacts' className='NavLink'>Back to contacts</NavLink>

                </div>

            </form>
        )
    }
}
const mapStateToProps = state => ({
    contacts: state.contacts
});

const mapDispatchToProps = dispatch => ({
    editContact: (key, contact) => dispatch(editContact(key, contact))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditContact);