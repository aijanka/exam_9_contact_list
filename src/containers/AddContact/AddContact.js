import React, {Component} from 'react';
import axios from 'axios';
import {NavLink} from "react-router-dom";
import './AddContact.css';
import {connect} from "react-redux";
import {loadContacts} from "../../store/actions";

class AddContact extends Component {

    state = {
        name: '',
        number: '',
        email: '',
        img: ''
    };

    saveCurrentValue = event => {
        event.preventDefault();
        this.setState({[event.target.name]: event.target.value});
    };

    addContact = () => {
        axios.post('/contacts.json', this.state);
        this.props.loadContacts();
        this.props.history.push('/contacts');
    };

    // toContacts = () => {
    //     this.props.history.push('/contacts');
    // };

    render () {
        return (
            <form className="AddContact">
                <div className="InputItem">
                    <label>Name</label>
                    <input
                        type="text"
                        name='name'
                        placeholder="Enter contact's name"
                        value={this.state.name}
                        onChange={this.saveCurrentValue}
                    />
                </div>



                <div className="InputItem">
                    <label>Phone number</label>
                    <input
                        type="number"
                        name='number'
                        placeholder="Enter contact's phone number"
                        value={this.state.number}
                        onChange={this.saveCurrentValue}
                    />
                </div>



                <div className="InputItem">
                    <label>Email</label>
                    <input
                        type="email"
                        name='email'
                        placeholder="Enter contact's email"
                        value={this.state.email}
                        onChange={this.saveCurrentValue}
                    />
                </div>



                <div className="InputItem">
                    <label>Image</label>
                    <input
                        type="text"
                        name='img'
                        placeholder="Enter contact's img"
                        value={this.state.img}
                        onChange={this.saveCurrentValue}
                    />
                </div>


                {this.state.img ? (<img className='ContactImg' src={this.state.img}/>) : <div className="emptyImg"/>}

                <div className="btns">
                    <button onClick={this.addContact}>Add contact</button>
                    {/*<button onClick={this.toContacts}>Back to contacts</button>*/}
                    <NavLink to='/contacts' className='NavLink'>Back to contacts</NavLink>

                </div>

            </form>
        )
    }
}


const mapDispatchToProps = dispatch => ({
    loadContacts: () => dispatch(loadContacts()),
});

export default connect(null, mapDispatchToProps)(AddContact);