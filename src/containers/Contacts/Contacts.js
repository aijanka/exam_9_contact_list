import React, {Component} from 'react';
import {connect} from "react-redux";
import Contact from "../../components/Contact/Contact";
import {deleteContact, loadContacts} from "../../store/actions";
import './Contacts.css';
import Modal from "../../components/UI/Modal/Modal";
import {NavLink} from "react-router-dom";

class Contacts extends Component {
    state = {
        showModal: false,
        currentKey: ''
    }

    componentDidMount() {
        this.props.loadContacts();
    }

    // componentDidUpdate(prevProps) {
    //     // if(this.props && this.props !== prevProps){
    //     //     this.props.loadContacts();
    //     // }
    // }

    toggleModal = () => {
        this.setState(prevState => ({showModal: !prevState.showModal}));
    }

    showFull = (event, key) => {
        event.preventDefault();
        this.setState({currentKey: key});
        this.toggleModal();
    };

    render() {
        let keys = this.props.contacts ? Object.keys(this.props.contacts) : [];
        const link = this.state.currentKey ? `/contacts/${this.state.currentKey}/editContact` : null;
        const c = this.props.contacts;
        return (
            <div className="ContactsWrapper">
                {this.state.currentKey ? (
                                                <Modal show={this.state.showModal} closed={this.toggleModal}>
                                                <div className="flex">
                                                    <div className="text">
                                                        <h2>{c[this.state.currentKey].name}</h2>
                                                        <p>{c[this.state.currentKey].number}</p>
                                                        <p>{c[this.state.currentKey].email}</p>
                                                    </div>
                                                    <img src={c[this.state.currentKey].img}/>
                                                </div>
                                                <div className="btns">
                                                    <button onClick={() => {
                                                        this.setState({currentKey: ''});
                                                        this.props.deleteContact(this.state.currentKey);
                                                        this.toggleModal();
                                                        // this.props.loadContacts();
                                                    }}>
                                                        delete
                                                    </button>
                                                    <NavLink to={link} exact>edit</NavLink>
                                                </div>
                                            </Modal>) : null}
                {keys.map(key => {
                    return(
                    <Contact
                        key={key}
                        name={c[key].name}
                        number={c[key].number}
                        email={c[key].email}
                        img={c[key].img}
                        showFull={(event) => this.showFull(event, key)}
                    />
                )})}
            </div>

        )
    }
}

const mapStateToProps = state => ({
    contacts: state.contacts
});

const mapDispatchToProps = dispatch => ({
    loadContacts: () => dispatch(loadContacts()),
    deleteContact: key => dispatch(deleteContact(key))
});

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);