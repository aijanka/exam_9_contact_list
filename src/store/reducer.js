import * as actionTypes from './actionTypes';

const initialState = {
    contacts: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.CONTACTS_REQUEST:
            return {...state, loading: true};
        case actionTypes.CONTACTS_SUCCESS:
            return {...state, loading: false, contacts: action.contacts};
        case actionTypes.CONTACTS_FAILURE:
            return {...state, loading: false, error: action.error};
        case actionTypes.DELETE_REQUEST:
            return {...state, loading: true};
        case actionTypes.DELETE_SUCCESS:
            return {...state, loading: false};
        case actionTypes.DELETE_FAILURE:
            return {...state, loading: false, error: action.error};
        case actionTypes.EDIT_SUCCESS:
            return {...state, contacts: {
                ...state.contacts,
                [action.key]: action.editedContact
            }};
        default :
            return state;
    }
};

export default reducer;