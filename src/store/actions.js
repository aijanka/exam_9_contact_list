import axios from 'axios';
import * as actionTypes from './actionTypes';

export const contactsRequest = () => ({type: actionTypes.CONTACTS_REQUEST});
export const contactsSuccess = contacts => ({type: actionTypes.CONTACTS_SUCCESS, contacts});
export const contactsFailure = error => ({type: actionTypes.CONTACTS_FAILURE, error});

export const loadContacts = () => {
    return (dispatch, getState) => {
        dispatch(contactsRequest());
        axios.get('/contacts.json').then(response => {
            dispatch(contactsSuccess(response.data));
        }, error => {
            dispatch(contactsFailure(error));
        })
    }
};

export const deleteRequest = () => ({type: actionTypes.DELETE_REQUEST});
export const deleteSuccess = () => ({type: actionTypes.DELETE_SUCCESS});
export const deleteFailure = error => ({type: actionTypes.DELETE_FAILURE, error});

export const deleteContact = key => {
    return (dispatch, getState) => {
        dispatch(deleteRequest());
        axios.delete(`contacts/${key}.json`).then(response => {
            dispatch(deleteSuccess());
            dispatch(loadContacts());
        }, error => {
            dispatch(deleteFailure(error));
        })
    }
};

export const editSuccess = (editedContact, key) => ({type: actionTypes.EDIT_SUCCESS, editedContact, key});
export const editContact = (key, contact) => {
    return (dispatch, getState) => {
        axios.put(`contacts/${key}.json`, contact).then(response => {
            console.log(response.data);
            dispatch(editSuccess(response.data, key));
        })
    }
}