import React from 'react';
import './Contact.css';
import {connect} from "react-redux";

const Contact = props => {
   return (
       <div className="Contact" onClick={props.showFull}>
           <img src={props.img}/>
           <div className="ContactText">
               <h3 className="ContactName">Name: {props.name}</h3>
               <p>Phone number: {props.number}</p>
               <p>Email: {props.email}</p>
           </div>
       </div>
   )
};
// const mapStateToProps = state => ({
//     contacts: state.contacts
// });

// const mapDispatchToProps = dispatch => ({
//    deleteContact: key => dispatch(deleteContact)
// });

// export default connect(mapStateToProps, mapDispatchToProps)(Contact);
export default Contact;