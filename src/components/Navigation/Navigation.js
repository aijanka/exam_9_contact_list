import React from 'react';
import {NavLink} from "react-router-dom";

import './Navigation.css';

const Navigation = props => (
    <nav className='Navigation'>
        <ul>
            <li><NavLink to='/contacts'>Contacts</NavLink></li>
            <li><NavLink to='/addContact'>Add a contact</NavLink></li>
        </ul>
    </nav>
);

export default Navigation;
